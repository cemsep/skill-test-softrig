import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private authService: AuthService, private router: Router) { }

  // tslint:disable-next-line:typedef
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const isAuthenticated = this.authService.isAuthenticated();
    isAuthenticated.subscribe(authenticated => {
      if (!authenticated) {
        this.router.navigate(['/unauthorized'], {
          queryParams: { returnUrl: state.url }
        });
      }
    });

    return isAuthenticated;
  }
}
