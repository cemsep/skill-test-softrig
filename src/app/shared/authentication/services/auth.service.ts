import { Injectable } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: any;

  constructor(private oidcSecurityService: OidcSecurityService) {
    this.oidcSecurityService.userData$.subscribe(user => {
      this.currentUser = user;
    });
  }

  login(): void {
    console.log('start login');
    this.oidcSecurityService.authorize();
  }

  refreshSessionCheckSession(): void {
    console.log('start refreshSession');
    this.oidcSecurityService.authorize();
  }

  forceRefreshSession(): void {
    this.oidcSecurityService.forceRefreshSession().subscribe((result) => console.log(result));
  }

  logout(): void {
    console.log('start logoff');
    this.oidcSecurityService.logoff();
  }

  getToken(): string {
    return this.oidcSecurityService.getToken();
  }

  getCompanyKey(): string {
    return this.currentUser.companyKey;
  }

  isAuthenticated(): Observable<boolean> {
    return this.oidcSecurityService.isAuthenticated$;
  }

  getUserData(): Observable<any> {
    return this.oidcSecurityService.userData$;
  }
}
