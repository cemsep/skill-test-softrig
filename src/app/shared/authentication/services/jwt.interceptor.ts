import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { finalize, map, switchMap, tap } from 'rxjs/operators';

// Interceptor for add JWT token to each and every http request goin out form the app to API
@Injectable({providedIn: 'root'})
export class JwtInterceptor implements HttpInterceptor {
  currentUser: any;
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getToken();
    if (token) {
      request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
            'Content-Type' : 'application/json'
          }
      });
    }

    return next.handle(request);
  }
}
