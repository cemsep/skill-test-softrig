import { Component, Input } from '@angular/core';
import { LayoutState } from '../../models/layout-state.model';

@Component({
    selector: 'app-loading-spinner',
    templateUrl: './loading-spinner.component.html',
    styleUrls: ['./loading-spinner.component.scss'],
})
export class LoadingSpinnerComponent {
    /// The corresponding state - this component will display when isAppLoading is true or the state-variable is undefined
    /// (defaults to undefined)
    @Input() state: LayoutState = undefined;

    /// Diameter of the loading-spinner (defaults to 32)
    @Input() diameter = 32;

    /// Set to true to customize styling (default to false)
    @Input() noStyling = false;

    constructor() {}
}
