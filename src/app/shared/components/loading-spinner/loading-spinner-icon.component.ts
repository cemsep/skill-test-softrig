import { Component, Input } from '@angular/core';
import { LayoutState } from '../..//models/layout-state.model';

@Component({
    selector: 'app-loading-spinner-icon',
    template: '<app-loading-spinner [state]="state" [diameter]="24" [noStyling]="true"></app-loading-spinner>',
})

/// Shortcut component for app-loading-spinner with default styling and diameter=24
export class LoadingSpinnerIconComponent {
    @Input() state: LayoutState;
}
