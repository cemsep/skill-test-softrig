import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './modules/material.module';
import { AuthConfigModule } from './authentication/auth-config.module';

import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { LoadingSpinnerIconComponent } from './components/loading-spinner/loading-spinner-icon.component';

import { AuthService } from './authentication/services/auth.service';
import { JwtInterceptor } from './authentication/services/jwt.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

const components = [
    ConfirmDialogComponent,
    LoadingSpinnerComponent,
    LoadingSpinnerIconComponent
];

const modules = [CommonModule, FormsModule, ReactiveFormsModule, MaterialModule, AuthConfigModule];

@NgModule({
    declarations: components,
    imports: modules,
    providers: [AuthService, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
    exports: [modules, components],
})
export class SharedModule {}
