import { HttpErrorResponse } from '@angular/common/http';

export class LayoutState {
    isLoading: boolean;
    error: boolean | string | HttpErrorResponse;

    /// Returns true if (not isAppLoading AND not error).
    get isPristine(): boolean {
        return !(this.isLoading || this.error);
    }

    /// Set isAppLoading to true and error to undefined
    startLoading(): void {
        this.isLoading = true;
        this.error = undefined;
    }

    /// Set isAppLoading to false
    stopLoading(): void {
        this.isLoading = false;
    }

    /// Set the error
    setError(error: boolean | string | HttpErrorResponse): void {
        this.error = error;
    }

    /// Set both isAppLoading and error to undefined
    reset(): void {
        this.isLoading = undefined;
        this.error = undefined;
    }
}
