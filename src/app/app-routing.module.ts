import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { NavigationComponent } from './modules/navigation/navigation.component';
import { UnauthorizedComponent } from './modules/unauthorized/unauthorized.component';

const routes: Routes = [
  {
    path: '',
    component: NavigationComponent,
    children: [
       {
        path: 'contacts',
        loadChildren: () => import('./modules/contact/contact.module').then((m) => m.ContactModule),
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'forbidden',
        component: UnauthorizedComponent
      },
      {
        path: 'unauthorized',
        component: UnauthorizedComponent
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
