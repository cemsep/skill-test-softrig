import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ContactService } from '../state/services/contact.service';
import { ContactQuery } from '../state/contact.query';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../../shared/components/confirm-dialog/confirm-dialog';
import { LayoutState } from '../../../shared/models/layout-state.model';
import { Contact } from '../state/models/contact.model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ContactListComponent implements OnInit {

  contacts$: Observable<Contact[]>;
  displayedColumns = ['id', 'name', 'address', 'phone'];
  expandedElement: any;
  originalList = [];
  searchedList = [];

  // Layout states
  states = {
    contacts: new LayoutState(),
    deleteContact: new LayoutState()
  };

  constructor(
    private contactQuery: ContactQuery,
    private contactService: ContactService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.contacts$ = this.contactQuery.allContacts$;
    this.contacts$.subscribe(contacts => {
      this.originalList = contacts;
      this.searchedList = [...this.originalList];
    })
    this.loadContacts();
  }

  loadContacts(): void {
    this.states.contacts.startLoading();
    this.contactService
      .getContacts()
      .pipe(finalize(() => this.states.contacts.stopLoading()))
      .subscribe({
        error: (error) => this.states.contacts.setError(error)
      });
  }

  deleteContact(contactId: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: {
        title: 'Delete Contact',
        message: 'Are you sure you want the delete Contact?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.states.deleteContact.startLoading();
        this.contactService
          .deleteContact(contactId)
          .pipe(finalize(() => this.states.deleteContact.stopLoading()))
          .subscribe({
              error: (error) => this.states.deleteContact.setError(error),
          });
      }
    });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value.toLowerCase();
    if (filterValue) {
      this.searchedList = this.originalList.filter(contact => {
        let stringToMatch = contact.ID + ' '
          + contact.Info.Name + ' ';

        if (contact.Info.InvoiceAddress) {
          if (contact.Info.InvoiceAddress.AddressLine1) {
            stringToMatch = stringToMatch + contact.Info.InvoiceAddress.AddressLine1 + ' ';
          }
        }

        if (contact.Info.DefaultPhone) {
          if (contact.Info.DefaultPhone.Number) {
            stringToMatch = stringToMatch + contact.Info.DefaultPhone.Number + ' ';
          }
        }
        return stringToMatch.toLowerCase().includes(filterValue);
      });
    } else {
      this.searchedList = this.originalList;
    }
  }

}
