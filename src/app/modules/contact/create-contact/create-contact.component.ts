import { Component, OnInit } from '@angular/core';
import { ContactService } from '../state/services/contact.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LayoutState } from '../../../shared/models/layout-state.model';
import { ContactForCreating } from '../state/models/contact-for-creating.model';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit {

  contactForm: FormGroup;

  // Layout state
  states = {
    createContact: new LayoutState(),
  };

  constructor(private contactService: ContactService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.contactForm = this.fb.group({
      name: [undefined, [Validators.required]],
      email: [undefined, [Validators.required, Validators.email]],
      countryCodePhone: [undefined, [Validators.required]],
      phone: [undefined, [Validators.required]],
      addressLine1: [undefined, [Validators.required]],
      addressLine2: [undefined],
      postalCode: [undefined, [Validators.required]],
      city: [undefined, [Validators.required]],
      country: [undefined, [Validators.required]],
      countryCode: [undefined, [Validators.required]],
      comment: [undefined]
    });
  }

  submitCreate(): void {
    if (this.contactForm.invalid) {
      return;
    }

    const contactFormData = this.contactForm.getRawValue();

    const newContact = {
      Info: {
        Name: contactFormData.name,
        InvoiceAddress: {
            AddressLine1: contactFormData.addressLine1,
            AddressLine2: contactFormData.addressLine2,
            City: contactFormData.city,
            Country: contactFormData.country,
            CountryCode: contactFormData.countryCode,
            PostalCode: contactFormData.postalCode,
          },
        DefaultPhone: {
            CountryCode: contactFormData.countryCodePhone,
            Number: contactFormData.phone,
          },
        DefaultEmail: {
            EmailAddress: contactFormData.email,
          }
      },
      Comment: contactFormData.comment
    } as ContactForCreating;

    this.states.createContact.startLoading();
    this.contactService
      .createContact(newContact)
      .pipe(finalize(() => this.states.createContact.stopLoading()))
      .subscribe({
        next: (contact) => this.router.navigateByUrl(`/contacts/${contact.ID}`),
        error: (error) => this.states.createContact.setError(error),
      });
  }

}
