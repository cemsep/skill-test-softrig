import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ContactService } from '../state/services/contact.service';
import { ContactQuery } from '../state/contact.query';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../../shared/components/confirm-dialog/confirm-dialog';
import { LayoutState } from '../../../shared/models/layout-state.model';
import { Contact } from '../state/models/contact.model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {

  contactId: string;
  contact$: Observable<Contact>;

  // Layout states
  states = {
    contact: new LayoutState(),
    deleteContact: new LayoutState()
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private dialog: MatDialog
  )
  { }

  ngOnInit(): void {
    this.contactId = this.route.snapshot.params.id;
    this.contact$ = this.contactQuery.singleContact$(this.contactId);

    this.loadContactData();
  }

  loadContactData(): void {
    this.states.contact.startLoading();
    this.contactService
        .getContact(this.contactId)
        .pipe(finalize(() => this.states.contact.stopLoading()))
        .subscribe({ error: (error) => this.states.contact.setError(error) });
  }

  deleteContact(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: {
        title: 'Delete Contact',
        message: 'Are you sure you want the delete Contact?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.states.deleteContact.startLoading();
        this.contactService
          .deleteContact(this.contactId)
          .pipe(finalize(() => this.states.deleteContact.stopLoading()))
          .subscribe({
            next: () => this.router.navigateByUrl('/contacts'),
            error: (error) => this.states.deleteContact.setError(error),
          });
      }
    });
  }
}
