import { Injectable } from '@angular/core';
import { ID, Order, QueryConfig, QueryEntity } from '@datorama/akita';
import { Contact } from './models/contact.model';
import { Observable } from 'rxjs';
import { ContactStore, IContactState } from './store/contact.store';

@Injectable({ providedIn: 'root' })
@QueryConfig({
    sortBy: 'ID',
    sortByOrder: Order.ASC,
})
export class ContactQuery extends QueryEntity<IContactState> {
    allContacts$ = this.selectAll();
    singleContact$ = (contactId: ID): Observable<Contact> => this.selectEntity(contactId);

    constructor(protected store: ContactStore) {
        super(store);
    }
}
