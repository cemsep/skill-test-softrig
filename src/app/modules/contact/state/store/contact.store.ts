import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Contact } from '../models/contact.model';

export interface IContactState extends EntityState<Contact> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'contact', idKey: 'ID' })
export class ContactStore extends EntityStore<IContactState> {
    constructor() {
        super();
    }
}
