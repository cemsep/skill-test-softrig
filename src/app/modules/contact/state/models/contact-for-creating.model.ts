export class ContactForCreating {
  Info: {
    Name: string,
    InvoiceAddress: {
      AddressLine1: string,
      AddressLine2?: string,
      City: string,
      Country: string,
      CountryCode: string,
      PostalCode: string
    },
    DefaultPhone: {
      CountryCode: string,
      Number: string,
    },
    DefaultEmail: {
      EmailAddress: string,
    }
  };
  Comment?: string;
}
