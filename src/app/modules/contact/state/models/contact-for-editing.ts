export class ContactForEditing {
  ID: number;
  Info: {
    ID: number,
    Name: string,
    InvoiceAddress: {
      ID: number,
      AddressLine1: string,
      AddressLine2?: string,
      City: string,
      Country: string,
      CountryCode: string,
      PostalCode: string
    },
    DefaultPhone: {
      ID: number,
      CountryCode: string,
      Number: string,
    },
    DefaultEmail: {
      ID: number,
      EmailAddress: string,
    }
  };
  Comment?: string;
}
