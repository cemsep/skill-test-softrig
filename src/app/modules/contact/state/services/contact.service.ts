import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ContactStore } from '../store/contact.store';
import { Contact } from '../models/contact.model';
import { ContactForCreating } from '../models/contact-for-creating.model';
import { ContactForEditing } from '../models/contact-for-editing';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient, private contactStore: ContactStore) { }

  getContacts(): Observable<Contact[]> {
    const endpoint = 'https://test-api.softrig.com/api/biz/contacts?expand=Info,Info.InvoiceAddress,Info.DefaultPhone&hateoas=false&top=10';
    return this.http.get(endpoint).pipe(
      tap((contacts: Contact[]) => {
        this.contactStore.set(contacts);
      })
    );
  }

  getContact(contactId: string): Observable<Contact>  {
    const endpoint = `https://test-api.softrig.com/api/biz/contacts/${contactId}?expand=Info,Info.InvoiceAddress,Info.DefaultPhone,Info.DefaultEmail,Info.DefaultAddress`;
    return this.http.get(endpoint).pipe(
      tap((contact: Contact) => {
        this.contactStore.upsert(contact.ID, contact);
      })
    );
  }

  createContact(contact: ContactForCreating): Observable<Contact> {
    const endpoint = 'https://test-api.softrig.com/api/biz/contacts';
    return this.http.post(endpoint, contact).pipe(
      tap((contact: Contact) => {
        this.contactStore.add(contact);
      })
    );
  }

  updateContact(contactId: string, contact: ContactForEditing): Observable<Contact> {
    const endpoint = `https://test-api.softrig.com/api/biz/contacts/${contactId}`;
    return this.http.put(endpoint, contact).pipe(
      tap((contact: Contact) => {
        this.contactStore.upsert(contact.ID, contact);
      })
    );
  }

  deleteContact(contactId: string): Observable<any> {
    const endpoint = `https://test-api.softrig.com/api/biz/contacts/${contactId}`;
    return this.http.delete(endpoint).pipe(
      tap(() => {
        this.contactStore.remove(contactId);
      })
    );
  }
}
