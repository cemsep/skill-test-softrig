import { Component, OnInit } from '@angular/core';
import { ContactService } from '../state/services/contact.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ContactQuery } from '../state/contact.query';
import { LayoutState } from '../../../shared/models/layout-state.model';
import { Contact } from '../state/models/contact.model';
import { ContactForEditing } from '../state/models/contact-for-editing';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {

  contactId: string;
  contact: Contact;
  contact$: Observable<Contact>;
  contactForm: FormGroup;

  // Layout state
  states = {
    contact: new LayoutState(),
    editContact: new LayoutState(),
  };

  constructor(
    private contactService: ContactService,
    private contactQuery: ContactQuery,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.initializeForm();

    this.contactId = this.route.snapshot.params.id;
    this.contact$ = this.contactQuery.singleContact$(this.contactId).pipe(
      tap((contact: Contact) => {
        if (contact) {
          this.contact = contact;
          const formData = {
            name: contact.Info.Name,
            email: contact.Info.DefaultEmail.EmailAddress,
            countryCodePhone: contact.Info.DefaultPhone.CountryCode,
            phone: contact.Info.DefaultPhone.Number,
            addressLine1: contact.Info.InvoiceAddress.AddressLine1,
            addressLine2: contact.Info.InvoiceAddress.AddressLine2 || undefined,
            postalCode: contact.Info.InvoiceAddress.PostalCode,
            city: contact.Info.InvoiceAddress.City,
            country:  contact.Info.InvoiceAddress.Country,
            countryCode: contact.Info.InvoiceAddress.CountryCode,
            comment: contact.Comment || undefined
          };
          this.contactForm.patchValue(formData, { emitEvent: false });
        }
      })
    );
    this.loadContactData();
  }

  initializeForm(): void {
    this.contactForm = this.fb.group({
      name: [undefined, [Validators.required]],
      email: [undefined, [Validators.required, Validators.email]],
      countryCodePhone: [undefined, [Validators.required]],
      phone: [undefined, [Validators.required]],
      addressLine1: [undefined, [Validators.required]],
      addressLine2: [undefined],
      postalCode: [undefined, [Validators.required]],
      city: [undefined, [Validators.required]],
      country: [undefined, [Validators.required]],
      countryCode: [undefined, [Validators.required]],
      comment: [undefined]
    });
  }

  loadContactData(): void {
    this.states.contact.startLoading();
    this.contactService
        .getContact(this.contactId)
        .pipe(finalize(() => this.states.contact.stopLoading()))
        .subscribe({ error: (error) => this.states.contact.setError(error) });
  }

  submitEdit(): void {
    if (this.contactForm.invalid) {
      return;
    }

    const contactFormData = this.contactForm.getRawValue();

    const updatedContact = {
      ID: this.contact.ID,
      Info: {
        ID: this.contact.Info.ID,
        Name: contactFormData.name,
        InvoiceAddress: {
            ID: this.contact.Info.InvoiceAddress.ID,
            AddressLine1: contactFormData.addressLine1,
            AddressLine2: contactFormData.addressLine2 || null,
            City: contactFormData.city,
            Country: contactFormData.country,
            CountryCode: contactFormData.countryCode,
            PostalCode: contactFormData.postalCode,
          },
        DefaultPhone: {
            ID: this.contact.Info.DefaultPhone.ID,
            CountryCode: contactFormData.countryCodePhone,
            Number: contactFormData.phone,
          },
        DefaultEmail: {
            ID: this.contact.Info.DefaultEmail.ID,
            EmailAddress: contactFormData.email,
          }
      },
      Comment: contactFormData.comment || null
    } as ContactForEditing;

    this.states.editContact.startLoading();
    this.contactService
      .updateContact(this.contactId, updatedContact)
      .pipe(finalize(() => this.states.editContact.stopLoading()))
      .subscribe({
        next: () => this.router.navigateByUrl(`/contacts/${this.contactId}`),
        error: (error) => this.states.editContact.setError(error),
      });
  }

}
