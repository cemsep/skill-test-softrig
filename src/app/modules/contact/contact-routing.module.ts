import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { CreateContactComponent } from './create-contact/create-contact.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
import { AuthGuardService } from '../../shared/authentication/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ContactListComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'create',
    component: CreateContactComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'edit/:id',
    component: EditContactComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ContactDetailsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
