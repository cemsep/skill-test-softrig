import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../shared/authentication/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isAuthenticated: boolean;
  userData$: Observable<any>;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.userData$ = this.authService.getUserData();
    this.authService.isAuthenticated()
      .subscribe(authenticated => this.isAuthenticated = authenticated);
  }

  login(): void {
    this.authService.login();
  }

}
