import { Component, OnInit } from '@angular/core';
import { OidcClientNotification } from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';
import { AuthService } from '../../shared/authentication/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  userDataChanged$: Observable<OidcClientNotification<any>>;
  userData$: Observable<any>;
  isAuthenticated$: Observable<boolean>;


  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.userData$ = this.authService.getUserData();
    this.isAuthenticated$ = this.authService.isAuthenticated();
  }

  login(): void {
    this.authService.login();
  }

  logout(): void {
    this.authService.logout();
  }

  navigateHome(): void {
    this.router.navigate(['/']);
  }

}
